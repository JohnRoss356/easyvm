__author__ = 'Jan Růžička'
__email__ = 'jan.ruzicka01@gmail.com'

__version__ = '2.5'

from easyvm.vm import *


def test():
    der_body = parse("""
        1)  load_r coeff
            load_r exp
            mul
            load_c 'x'
            add
            load_r exp
            load_c 1
            sub
            dup
            dup
            load_c 1
            gt
            load_co
            load_c 0
            lt
            or
            pop_jump_true 2
            pop_jump 3

        2)  load_c '^'
            add
            load_co
            add

        3)  return
    """)

    main = parse("""
        1)  load_c der
            load_c coeff
            load_c exp
            tuple 2
            load_b
            func

        2)  load_c print
            load_c der
            load_c 2
            load_c 3
            tuple 2
            call
            tuple 1
            call
    """)

    b_push(der_body)

    run(main)


if __name__ == '__main__':
    test()