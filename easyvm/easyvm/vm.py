__author__ = 'Jan Růžička'
__email__ = 'jan.ruzicka01@gmail.com'

__version__ = '2.5'

import re
from numbers import Real


__safe_functions__ = ['push', 'pop']

nest = 0


def _hashable(lst):
    out = ()

    for x in lst:
        if type(x) is list:
            out += (_hashable(x),)
        else:
            out += (x,)

    return out


class OpCode:
    opcodes = {
        "NOP": 0,
        "LOAD_C": 1,
        "LOAD_R": 2,
        "STORE_R": 3,
        "LOAD_B": 4,
        "TUPLE": 5,
        "CLASS": 6,
        "FUNC": 7,
        "CALL": 8,
        "ADD": 9,
        "SUB": 10,
        "MUL": 11,
        "DIV": 12,
        "POW": 13,
        "ROOT": 14,
        "LIST": 15,
        "EQ": 16,
        "JUMP": 17,
        "POP_JUMP": 18,
        "POP_JUMP_TRUE": 19,
        "RETURN": 20,
        "LOAD_A": 21,
        "UNPACK_SEQUENCE": 22,
        "STORE_A": 23,
        "UNREF": 24,
        "PAIR": 25,
        "LT": 26,
        "GT": 27,
        "LE": 28,
        "GE": 29,
        "NE": 30,
        "GLOBAL": 31,
        "DUP": 32,
        "LOAD_CO": 33,
        "AND": 34,
        "OR": 35,
        "NOT": 36,
        "XOR": 37,
    }

    @staticmethod
    def replace_by_token(instruct):
        a = OpCode.opcodes[instruct[0].upper()]

        if len(instruct) > 1:
            return a, instruct[1]
        else:
            return a,


class ID:
    def __init__(self, name):
        self.name = name

    def __hash__(self):
        return hash(self.name)


class KeywordArgument:
    def __init__(self, key, name):
        self.key = key
        self.name = name

    def __hash__(self):
        return hash(str(hash(self.key)) + str(hash(self.name)))

    def __str__(self):
        return self.key + ': ' + self.name


class SequenceArgument:
    def __init__(self, name):
        self.name = name

    def __hash__(self):
        return hash(self.name)

    def __str__(self):
        return '*' + self.name


class Function:
    def __init__(self, name, args):
        self.name = name
        self.args = list(args)

        self.a_count = [0, 0]

        for x in args:
            if type(x) is SequenceArgument:
                self.a_count[0] = -1

            elif type(x) is KeywordArgument:
                self.a_count[1] += 1

            elif self.a_count[0] != -1:
                self.a_count[0] += 1

        self.a_count = tuple(self.a_count)

    def flatten_args(self):
        out = []

        for x in self.args:
            out.append(str(x))

        return out

    def __eq__(self, other):
        assert type(other) is Function, 'Expecting `Function`, found `%s`!' % type(other)

        return self.name == other.name and (self.a_count[0] == other.a_count[0] or self.a_count[0] == -1
                                            or other.a_count[0] == -1) and self.a_count[1] == other.a_count[1]

    def __hash__(self):
        return hash(self.name)

    def __str__(self):
        return self.name + '(' + str(self.flatten_args())[1:-1] + ')'


class Closure:
    def __init__(self, args, body):
        self.args = args
        self.body = body

    def run(self, args):
        global nest

        nest += 1
        refs[nest] = {}

        args = list(args)

        for a in self.args:
            if type(a) is SequenceArgument:
                o = []

                x = None
                while len(args) > 0 and type(x) is not tuple:
                    x = args[0]
                    del args[0]

                    o.append(x)
                refs[nest][a.name] = tuple(o)

            elif type(a) is KeywordArgument:
                keys = [x for x in args if type(x) is KeywordArgument and x.key == a.key]

                assert len(keys) == 1, 'Expecting a keyword argument `%s`, none found, or multiple found!' % a.key

                val = keys[0].name

                refs[nest][a.name] = val

            else:
                refs[nest][a] = args[0]
                del args[0]

        if len(self.args) == 0:
            run(self.body)
        else:
            run(self.body)

        del refs[nest]

        nest -= 1


class Class:
    def __init__(self, name, super):
        assert re.match(r'[_a-zA-Z][_a-zA-Z0-9]*', name), '`%s` is not a valid identifier!' % name
        assert super is None or \
            (re.match(r'[_a-zA-Z][_a-zA-Z0-9]*', super) and super in refs and isinstance(refs[super], dict)), \
            '`%s` is not valid class!' % super

        self.name = name
        self.super = super

    def __str__(self):
        return self.super + ' > ' + self.name if self.super else self.name


class ClassBody(Closure):
    cache = {}

    def __init__(self, body):
        super(ClassBody, self).__init__((), body)

        self.body = body

        if _hashable(self.body) not in ClassBody.cache:
            self.dir = self.make_dir()
            ClassBody.cache[_hashable(self.body)] = self.dir

            for x in self.dir:
                if type(self.dir[x]) is Closure:
                    if not hasattr(self.dir[x].body, '__call__'):
                        self.dir[x].body.insert(0, [(1, '__self__'), (push, self.copy()), (3,)])

                        if self.dir[x].body[-1][-1] != (20,):
                            self.dir[x].body.append([(2, '__self__'), (24, '__self__')])
                        else:
                            self.dir[x].body.append([(24, '__self__')])
        else:
            self.dir = ClassBody.cache[_hashable(self.body)]

    def load_a(self, a):
        assert type(a) is str, 'Invalid id type `%s`!' % type(a)
        assert re.match(r'[_a-zA-Z][_a-zA-Z0-9]*', a), 'Invalid id `%s`!' % a

        for x in self.dir:
            if type(x) is Function:
                if x.name == a:
                    return self.dir[x]

            elif x == a:
                return self.dir[x]

        raise NameError('No such attribute `%s`!' % a)

    def store_a(self, name, val):
        self.dir[name] = val

    def make_dir(self):
        _old = refs[-1].copy()

        run(self.body)

        _to_del = dict_diff(refs[-1], _old)

        for x in _to_del:
            del refs[-1][x]

        return _to_del

    def copy(self):
        return ClassBody(self.body)


def dict_diff(d1, d2):
    diff = set(d1.keys()) - set(d2.keys())

    out = {}

    for x in diff:
        out[x] = d1[x]

    return out


def type_check(c):
    if c == '()':
        return None
    elif type(c) is not str:
        return c

    string = re.match(r'"(?:[^\\"]|\\.)*"', c)
    ident = re.match(r'[_a-zA-Z][_a-zA-Z0-9]*', c)
    number = re.match(r'[+-]?[0-9]+(\.[0-9]+)?', c)
    boolean = re.match(r'true|false', c)

    if not string:
        string = re.match(r'\'(?:[^\\\']|\\.)*\'', c)

    assert bool(string) | bool(ident) | bool(number), "`%s` is not valid value!" % c

    if number:
        try:
            c = int(c)
        except:
            c = float(c)
    elif string:
        c = c[1:-1]
    elif boolean:
        c = c == 'true'
    elif ident:
        c = ID(c)

    return c


def nroot(k, n):
    u, s = n, n + 1
    while u < s:
        s = u
        t = (k - 1) * s + n / (s ** (k - 1))
        u = t / k
    return s


def flip():
    a, b = pop(), pop()

    push(a)
    push(b)


def pop():
    if len(stack) == 0:
        return None

    a = stack[0]
    del stack[0]

    return a


def push(v):
    stack.insert(0, v)


def b_pop():
    if len(bstack) == 0:
        return None

    a = bstack[0]
    del bstack[0]

    return a


def b_push(b):
    if type(b) is str:
        b = parse(b)

    bstack.insert(0, b)


def load_c(c):
    push(c)


def load_a(name):
    obj = pop()

    assert type(name) is str and re.match(r'[_a-zA-Z][_a-zA-Z0-9]*', name), 'Expecting id, not `%s`!' % name

    if type(obj) is ClassBody:
        push(obj.load_a(name))
        return

    assert type(obj) is str and re.match(r'[_a-zA-Z][_a-zA-Z0-9]*', obj), 'Expecting id, not `%s`!' % obj

    for x in refs[-1]:
        if ((isinstance(x, Class) and x.name == obj) or (type(x) is str and x == obj)) \
                and isinstance(refs[-1][x], ClassBody):
            push(refs[-1][x].load_a(name))
            return

    raise NameError('No such object `%s`!' % obj)


def load_r(name):
    assert type(name) is ID, "Expecting ID, found `%s`!" % type(name)

    name = name.name

    try:
        assert name in refs[nest]

        v = refs[nest][name]
    except:
        try:
            assert name in refs[-1], "Invalid pointer `%s`!" % name.name

            v = refs[-1][name]
        except:
            assert ID('$' + name.name) in refs[nest], 'Invalid pointer `%s`!' % name.name

            v = refs[0][name]

    push(v)


def store_r():
    v = pop()
    n = pop()

    assert type(n) in {tuple, ID}, 'Expecting id or sequence, not `%s`!' % type(n)

    if type(n) is tuple:
        assert type(v) is tuple, 'Only can assign two sequences, not seq and `%s`!' % type(v)
        assert len([x for x in n if type(x) is not ID]) == 0, 'Expecting only ids in sequence!'

        i = 0
        for x in n:
            push(x)

            if i < len(v):
                push(v[i])

                i += 1
            else:
                push(v[-1])

        for _ in range(len(n)):
            store_r()
        return

    assert type(n) is ID, '`%s` is not valid identifier!' % n

    n = n.name

    if ID('$' + n) in refs[nest]:
        refs[0][n] = v
    else:
        refs[nest][n] = v


def load_b():
    return b_pop()


def _tuple(n):
    t = []
    for i in range(n):
        t.append(pop())

    t.reverse()

    push(tuple(t))


def _list(n):
    t = []
    for i in range(n):
        t.append(pop())

    t.reverse()

    push(t)


def pair():
    flip()
    push(KeywordArgument(pop(), pop()))


def eq():
    flip()
    a, b = pop(), pop()

    try:
        a.load_a('__eq__').run((b,))

    except (NameError, AttributeError):
        try:
            b.load_a('__req__').run((a,))

        except:
            push(a == b)


def lt():
    flip()
    a, b = pop(), pop()

    try:
        a.load_a('__lt__').run((b,))

    except (NameError, AttributeError):
        try:
            b.load_a('__rlt__').run((a,))

        except:
            push(a < b)


def gt():
    flip()
    a, b = pop(), pop()

    try:
        a.load_a('__gt__').run((b,))

    except (NameError, AttributeError):
        try:
            b.load_a('__rgt__').run((a,))

        except:
            push(a > b)


def le():
    flip()
    a, b = pop(), pop()

    try:
        a.load_a('__le__').run((b,))

    except (NameError, AttributeError):
        try:
            b.load_a('__rle__').run((a,))

        except:
            push(a <= b)


def ge():
    flip()
    a, b = pop(), pop()

    try:
        a.load_a('__ge__').run((b,))

    except (NameError, AttributeError):
        try:
            b.load_a('__rge__').run((a,))

        except:
            push(a >= b)


def ne():
    flip()
    a, b = pop(), pop()

    try:
        a.load_a('__ne__').run((b,))

    except (NameError, AttributeError):
        try:
            b.load_a('__rne__').run((a,))

        except:
            push(a != b)


def _and():
    a, b = pop(), pop()

    push(a and b)


def _or():
    a, b = pop(), pop()

    push(a or b)


def _not():
    push(not pop())


def _xor():
    a, b = pop(), pop()

    push(bool(a) != bool(b))


def add():
    flip()
    a, b = pop(), pop()

    try:
        a.load_a('__add__').run((b,))

    except (NameError, AttributeError):
        try:
            b.load_a('__radd__').run((a,))

        except:
            if type(a) != type(b):
                if type(a) is str:
                    b = str(b)
                elif type(a) is list:
                    b = [b]
                elif type(a) is tuple:
                    o = []
                    for x in a:
                        push(x)
                        push(b)
                        add()

                        o.append(pop())
                    push(tuple(o))
                    return
                elif issubclass(type(a), Real):
                    if type(b) is str:
                        try:
                            b = int(b)
                        except:
                            try:
                                b = float(b)
                            except:
                                a = str(a)

            push(a + b)


def sub():
    flip()
    a, b = pop(), pop()

    try:
        a.load_a('__sub__').run((b,))

    except (NameError, AttributeError):
        try:
            b.load_a('__rsub__').run((a,))

        except:
            if type(a) != type(b):
                if type(a) is str:
                    if issubclass(type(b), Real):
                        push(a[:-b])
                        return
                elif type(a) is list:
                    b = [b]
                elif type(a) is tuple:
                    o = []
                    for x in a:
                        push(x)
                        push(b)
                        sub()

                        o.append(pop())
                    push(tuple(o))
                    return
                elif issubclass(type(a), Real):
                    if type(b) is str:
                        try:
                            b = int(b)
                        except:
                            b = float(b)
            if type(a) is list:
                push([x for x in a if x not in b])
                return

            push(a - b)


def mul():
    flip()
    a, b = pop(), pop()

    try:
        a.load_a('__mul__').run((b,))

    except (NameError, AttributeError):
        try:
            b.load_a('__rmul__').run((a,))

        except:
            if type(a) != type(b):
                if type(a) is list:
                    b = [b]
                elif type(a) is tuple:
                    o = []
                    for x in a:
                        push(x)
                        push(b)
                        mul()

                        o.append(pop())
                    push(tuple(o))
                    return
                elif issubclass(type(a), Real):
                    if type(b) is str:
                        try:
                            b = int(b)
                        except:
                            b = float(b)
            if type(a) is list:
                push([x for x in a if x in b])
                return

            push(a * b)


def div():
    flip()
    a, b = pop(), pop()

    try:
        a.load_a('__div__').run((b,))

    except (NameError, AttributeError):
        try:
            b.load_a('__rdiv__').run((a,))

        except:
            if type(a) != type(b):
                if type(a) is list:
                    b = [b]
                elif type(a) is tuple:
                    o = []
                    for x in a:
                        push(x)
                        push(b)
                        div()

                        o.append(pop())
                    push(tuple(o))
                    return
                elif issubclass(type(a), Real):
                    if type(b) is str:
                        try:
                            b = int(b)
                        except:
                            b = float(b)
            if type(a) is list:
                push([x for x in b if x not in a] + [x for x in a if x not in b])
                return

            push(a / b)


def pow():
    flip()
    a, b = pop(), pop()

    try:
        a.load_a('__pow__').run((b,))

    except (NameError, AttributeError):
        try:
            b.load_a('__rpow__').run((a,))

        except:
            if type(a) != type(b):
                if type(a) is tuple:
                    o = []
                    for x in a:
                        push(x)
                        push(b)
                        add()

                        o.append(pop())
                    push(tuple(o))
                    return
                elif issubclass(type(a), Real):
                    if type(b) is str:
                        try:
                            b = int(b)
                        except:
                            b = float(b)

            push(a ** b)


def root():
    a, b = pop(), pop()

    try:
        a.load_a('__root__').run((b,))

    except (NameError, AttributeError):
        try:
            b.load_a('__rroot__').run((a,))

        except:
            if type(a) != type(b):
                if type(a) is tuple:
                    o = []
                    for x in a:
                        push(x)
                        push(b)
                        root()

                        o.append(pop())
                    push(tuple(o))
                    return
                elif issubclass(type(a), Real):
                    if type(b) is str:
                        try:
                            b = int(b)
                        except:
                            b = float(b)

            push(nroot(a, b))


def format_args(args):
    args = list(args)

    for i, a in enumerate(args):
        if type(a) is list and len(a) == 1 and type(a[0]) is ID:
            args[i] = SequenceArgument(a[0].name)
        else:
            args[i] = a.name

    return tuple(args)


def call():
    args, name = pop(), pop()

    assert type(args) is tuple, 'Expecting tuple as arguments!'

    if type(name) is Closure:
        name.run(args)
        return

    elif hasattr(name, '__call__'):
        name(*args)
        return

    assert type(name) is ID, 'Expecting ID, found `%s`!' % type(name)

    name = name.name

    r = None
    _refs_copy = tuple((x, y) for x, y in refs[-1].items())

    for x in _refs_copy:
        try:
            if Function(name, args) == x[0]:
                if type(x[1]) is Closure:
                    r = x[1].run(args)

                elif hasattr(x[1], '__call__'):
                    r = x[1](*args)

                else:
                    raise TypeError('Invalid closure type!')

                break

        except AssertionError:
            pass

        except:
            raise

    if r:
        push(r)


def func():
    body, args, name = pop(), format_args(pop()), pop()

    assert type(name) is ID, 'Expecting ID, found `%s`!' % type(name)

    name = name.name

    refs[-1][Function(name, args)] = Closure(args, body)


def _class():
    body, super_c, name = ClassBody(pop()), pop(), pop()

    assert type(name) is ID, 'Expecting ID, found `%s`!' % type(name)
    assert type(super_c) is ID or super_c is None, 'Expecting ID, found `%s`!' % type(super_c)

    name = name.name

    if super_c:
        super_c = super_c.name

    c = Class(name, super_c)
    refs[-1][c] = body

    _constructor = Function(name, ())
    _constructor_body = Closure((), [[(push, refs[-1][c])]])

    _str = Function('to_s', ())
    _str_body = Closure((), [[(1, '\'<class ' + name + '>\'')]])

    _load_self = [(1, '__self__'), (push, refs[-1][c]), (3,)]

    try:
        refs[-1][c].load_a('__init__')

        head = Function(name, refs[-1][c].load_a('__init__').args)
        body = refs[-1][c].load_a('__init__')

        refs[-1][head] = body

    except NameError:
        refs[-1][_constructor] = _constructor_body

    try:
        refs[-1][c].load_a('to_s')

    except NameError:
        refs[-1][c].dir[_str] = _str_body


def store_a():
    value, attr, obj = type_check(pop()), pop(), pop()

    assert re.match(r'[_a-zA-Z][_a-zA-Z0-9]*', obj), 'Expecting id, not `%s`!' % obj
    assert re.match(r'[_a-zA-Z][_a-zA-Z0-9]*', attr), 'Expecting id, not `%s`!' % attr
    assert obj in refs[-1], 'Invalid pointer `%s`!' % obj
    assert type(refs[-1][obj]) is ClassBody, 'Expecting `ClassBody`, not `%s`!' % type(refs[-1][obj])

    refs[-1][obj].store_a(attr, value)


def unref(name):
    assert type(name) is ID, 'Expecting ID, not `%s`!' % type(name)

    name = name.name

    assert name in refs[nest], 'Invalid pointer `%s`!' % name

    del refs[nest][name]


def unpack_sequence():
    seq = pop()

    if type(seq) not in [list, tuple]:
        seq = [seq]

    i = 0
    for x in list(seq):
        push(x)
        i += 1


def _global(name):
    assert type(name) is str, ' Invalid type `%s`!' % type(name)
    assert re.match(r'[_a-zA-Z][_a-zA-Z0-9]*', name), 'Expecting id!'
    assert name in refs[0], 'Invalid pointer `%s`!' % name

    refs[nest]['$' + name] = refs[0][name]


def _return():
    push(pop())


def dup():
    costack.insert(0, stack[0])


def load_co():
    if len(costack) == 0:
        return None
    a = costack[0]
    del costack[0]
    return a


# Shorthand for the 'run' function.
codes = {
    0: lambda: None,
    1: load_c,
    2: load_r,
    3: store_r,
    4: load_b,
    5: _tuple,
    6: _class,
    7: func,
    8: call,
    9: add,
    10: sub,
    11: mul,
    12: div,
    13: pow,
    14: root,
    15: _list,
    16: eq,
    20: _return,
    21: load_a,
    22: unpack_sequence,
    23: store_a,
    24: unref,
    25: pair,
    26: lt,
    27: gt,
    28: le,
    29: ge,
    30: ne,
    31: _global,
    32: dup,
    33: load_co,
    34: _and,
    35: _or,
    36: _not,
    37: _xor,
}


# Operating stack - OS.
stack = []


# Co-stack, used to store primitive temp variables.
costack = []


# Block stack - BS.
bstack = []


def format(args):
    a = ''
    for x in args:
        if type(x) is ClassBody:
            try:
                x.load_a('to_s').run(())
                x = pop()

            except NameError:
                raise NameError('Object without `to_s` function!')

        elif type(x) in [list, tuple]:
            a += '['
            for i in x:
                a += format([i]) + ', '
            a = a[:-2] + '] '
            continue

        a += str(x) + ' '

    return a[:-1]


# Builtin functions
def __printfunc__(*args, **keys):
    a = format(args)

    print(a)
    return a


def __inputfunc__(prompt):
    return input(prompt)


# Reference table

# Superglobal scope is -1, global scope is 0, local scopes are the other.

refs = {0: {}, -1: {Function('print', (SequenceArgument('args'),)): __printfunc__, Function('input', ('prompt',)): __inputfunc__}}


def strip_opcode(src):
    src = re.sub(r" +", " ", src).lstrip(" \n\t\r").rstrip(" \n\t\r")

    return src


def parse(src):
    src = [x for x in [strip_opcode(x) for x in re.split(r"\n+", strip_opcode(src))] if bool(x)]

    op_sects = []

    last_sect = 0
    sect = []

    for x in src:
        x = strip_opcode(x)

        if x[:2] == str(last_sect + 1) + ")":
            if sect:
                op_sects.append(sect)

            sect = [strip_opcode(x[2:])]
            last_sect += 1
        else:
            sect.append(x)

    if sect:
        op_sects.append(sect)

    for i, y in enumerate(op_sects):
        for j, x in enumerate(y):
            op_sects[i][j] = OpCode.replace_by_token(tuple(x.split(' ', 1)))

    return op_sects


def _interpret(ins):
    if len(ins) > 1:
        a = type_check(ins[1])
        return codes[ins[0]](a)
    else:
        return codes[ins[0]]()


def run(op, debug=False):
    global stack

    if hasattr(op, '__call__'):
        op()
        return

    a = None
    s_skip = 0
    for i, sect in enumerate(op):
        i += 1

        if s_skip > 0:
            s_skip -= 1

            if s_skip > 0:
                continue

        for x in sect:
            if hasattr(x[0], '__call__'):
                x[0](x[1])
                continue

            if x[0] in range(17, 20):
                if x[0] == 17:
                    s_skip = int(x[1]) - i
                    break

                elif x[0] == 18:
                    pop()
                    s_skip = int(x[1]) - i
                    break

                elif x[0] == 19:
                    if len(stack) > 0 and bool(stack[0]):
                        pop()
                        s_skip = int(x[1]) - i
                        break
                continue

            r = _interpret(x)

            if r:
                push(r)

            a = stack[0] if len(stack) > 0 else None

    if a and debug:
        print(a)