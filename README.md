# EasyVM
EasyVM - Easy-to-use Virtual Machine.    
EasyVM serves you simple bytecode format which is similar to the Python's one, but with simpler syntax.

## Bytecode
EasyVM bytecode contains these opcode instructions:

### Basic: ###

  * `nop` - Do nothing.
  * `load_c (c)` - Push constant. (id, string, number)
  * `load_r (r)` - Push reference value.
  * `store_r` - Store 1st element of stack to reference of name 2nd element of stack and pop them.
  * `tuple (n)` - Round `n` elements of stack into tuple, pop them, reverse tuple, and push it.
  * `list (n)` - Round `n` elements of stack into list, pop them, reverse list, and push it.
  * `pair` - Create KeywordArgument of two-top elements and push it.
  * `unpack_sequence` - Pop 1st element (usually tuple) and push its each element separately to the stack.
  * `call` - Call 2nd element of stack, as name, with 1st element (tuple) of stack, as arguments, and push returned value.
  * `load_b` - Load block from block stack and push it to main stack.
  
### Arithmetic operators - All will first flip two top elements of stack: ###

  * `add` - Add two top elements of stack, pop them, and push the result.
  * `sub` - Subtract -----------------------------||------------------------------.
  * `mul` - Multiply ------------------------------||------------------------------.
  * `div` - Divide --------------------------------||------------------------------.
  * `pow` - Power --------------------------------||------------------------------.
  * `root` - Root ---------------------------------||------------------------------.
  
### Comparison operators - All will first flip two top elements of stack: ###

  *   `eq` - Push `True` if two top elements are equal.
  *   `lt` - Push `True` if second element is less than 1st.
  *   `gt` - Push `True` if second element is greater than 1st.
  *   `le` - Push `True` if second element is less than 1st or they're equal.
  *   `ge` - Push `True` if second element is greater than 1st or they're equal.
  *   `ne` - Push `True` if two top elements are different.

### Blocks: ###

  * `func` - Store function under name as 3rd element of stack, with parameters as 2nd element of stack (tuple) and body as 1st element of stack (bytecode AST).
  * `class` - Builds class from block, superclass and name (in that order). Block will be executed and initialized variables will be saved to class' `dir`. Special `to_s` and constructors will be created automatically.

### Easy Magic
#### Magic Methods (used by builtin functions): ####

* `__add__`, `__sub__`, `__mul__`, `__div`, `__pow__`, `__root__` - Define behavior for the above arithmetic operations.
* `__str__` - Called by `print`.
* `__init__` - Exported to global scope with name of the class.
* `__eq__`, `__lt__`, `__gt__`, `__le__`, `__ge__`, `__ne__` - Define behavior for the above comparison operators.
* `__r...__` - Reversed operation, called when magic method is not present in first operand.

##### Upcoming magic methods: #####

* `__repr__` - EasyVM valid representation.

#### Magic Object: ####

* `__self__` - Automatically added at first of each method body and deleted at the end. If method `return` is not present, it will be returned.

### Examples:
#### Sample "Hello world!" program: ####

```
1)  load_c print
    load_c 'Hello world!'
    tuple 1
    call
```

#### Basic arithmetic (1 + 2 - 3 * 4 / 5 ** 6 // 7, '//' is nth root(number, nth)): ####

```
1)  load_c 1
    load_c 2
    add
    load_c 3
    load_c 4
    mul
    load_c 5
    load_c 6
    load_c 7
    root
    pow
    div
    sub
```

#### Function create: ####
##### Function body: #####
    
```
1)  load_c print
    load_r what
    tuple 1
    unpack_sequence
    call
```


##### Main Program: #####

```
1)  load_c say
    load_c what
    list 1
    tuple 1
    load_b
    func

2)  load_c say
    load_c 'Hello world!'
    load_c 'Hello User!'
    tuple 2
    call
```
*Creating one-length lists on function creation will mark this argument as sequence argument. Keyword arguments use a `pair` instruction.*
  
   * We need to push the block ontop the block-stack:
    `b_push(our_function_block)`
  
   * And run our program.

***!WARNING::When loading block with `load_b` without block pushed on block stack will result in strange things! It's not VM's fault, just push the blocks in well order or don't load them!***

##### Extension Function: #####
You can create an EasyVM function from Python closure. It's called extension function. However, you can't use EasyVM declared variables inside.

###### Example: ######
**Python function:**
```
    def foo():
        print('Hello world!')
```

**Main program:**
```
    1)  load_c foo
        tuple 0
        load_b
        func

    2)  load_c foo
        tuple 0
        call
```

**Then push the block:**
```
    b_push(foo)
```

And run the main program.